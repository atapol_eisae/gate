// STM32 Initialization
#include "main.h"

/*
static uint8_t prohibitive_card_id[PROHIBITIVE_CARD_NUM][CARD_ID_SIZE] = {
							{0xBE,0xBD,0x03,0x04},
							{0xFE,0xBD,0x03,0x01},
							{0xBE,0xBD,0x03,0x01},
							{0xFE,0xBD,0x03,0x00},
							{0xFE,0xBD,0x08,0x01},
							{0xFE,0xBD,0x03,0x40},
							{0xBE,0xBD,0x03,0x00}
};
*/

static main_control_var_t main_control;
static usart_var_t main_usart;

void Delay_ms(unsigned int nCount);		/* Private function */
GPIO_InitTypeDef GPIO_InitStructure;	/* Private typedef */

//=============================================================================
// TIM3 Interrupt Handler
//=============================================================================
void TIM3_IRQHandler(void)
{
	if(TIM3->SR & TIM_SR_UIF) // if UIF flag is set
  {
		TIM3->SR &= ~TIM_SR_UIF; // clear UIF flag
		if(main_control.led_status == 0)
		{
			GPIO_ResetBits(LED_STATUS_PORT,LED_STATUS_PIN);
			//GPIO_ResetBits(USR_RESET_PORT,USR_RESET_PIN);
			main_control.led_status = 1;
		}
		else
		{
				GPIO_SetBits(LED_STATUS_PORT,LED_STATUS_PIN);
				//GPIO_SetBits(USR_RESET_PORT,USR_RESET_PIN);
				main_control.led_status = 0;
		}
#ifdef	AUTO_READ_CARD_MODE
		main_control.read_card_flag = 1;
#endif
  }
}

static void timer_initialize(void)
{
		RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPCEN;
		RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
		
		TIM3->PSC = 23999;	        // Set prescaler to 24 000 (PSC + 1)
		TIM3->ARR = 3000;	          // Auto reload value 3000 (1 second)
		TIM3->DIER = TIM_DIER_UIE; // Enable update interrupt (timer level)
		TIM3->CR1 = TIM_CR1_CEN;   // Enable timer

		NVIC_EnableIRQ(TIM3_IRQn); // Enable interrupt from TIM3 (NVIC level)
	
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);	/* GPIOC Periph clock enable */
}

static void main_gpio_initialize(void)
{
  //LED Pin configuration
	GPIO_InitStructure.GPIO_Pin = LED_STATUS_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LED_STATUS_PORT, &GPIO_InitStructure);	/* Configure PC13 in output pushpull mode */
	//USR Reset Pin configuration
	GPIO_InitStructure.GPIO_Pin = USR_RESET_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(USR_RESET_PORT, &GPIO_InitStructure);	/* Configure PC13 in output pushpull mode */
}
/*
static uint8_t card_id_verify(void)
{
	uint8_t card_idx,byte_idx,id_byte_match;
	uint8_t prohibitive_flag = 0;
	
	for(card_idx=0;card_idx<PROHIBITIVE_CARD_NUM;card_idx++)
	{
			id_byte_match = 1;
			for(byte_idx=0;byte_idx<CARD_ID_SIZE;byte_idx++)
			{
					if(main_usart.buff[byte_idx + CARD_ID_OFFSET_BYTE] != prohibitive_card_id[card_idx][byte_idx])
					{
							id_byte_match = 0;
							break;							
					}
			}
			if(id_byte_match)
			{
					prohibitive_flag = 1;
			}
	}
	return(!prohibitive_flag);
}
*/

static uint8_t card_id_verify(void)
{
	uint8_t card_idx,byte_idx,id_byte_match;
	uint8_t verify_status = NO_CARD_STATUS;
	char buff[50];
	
	if(main_usart.buff[0] == (char)CARD_CMD_RESPONSIVE_HEADER)
	{
			if(main_usart.buff[2] == (char)READ_CARD_ID_CMD)
			{
				if(main_usart.buff[3] == (char)OPERATION_SUCCESS)
				{
						verify_status = VALID_CARD_ID_STATUS;
				}
				else
				{
						verify_status = NO_CARD_STATUS;
				}
			}
			else
			{
					verify_status = OTHER_CMD_STATUS;
			}
	}
	else
	{
			verify_status = INVALID_HEADER_STATUS;
	}
	
	return(verify_status);
}

static void Delay_ms(unsigned int nCount){ 
	unsigned int i, j;
	for(i = 0; i < nCount; i++)
	{  
		for(j = 0; j < 0x2AFF; j++) {;} 
  }
} 

static void usr_k3_reset(void)
{
	GPIO_ResetBits(USR_RESET_PORT,USR_RESET_PIN);
	Delay_ms(200);
	GPIO_SetBits(USR_RESET_PORT,USR_RESET_PIN);
}

static uint8_t main_state_machine(void)
{
	switch(main_control.main_state)
	{
		case MAIN_INIT_STATE:
			main_gpio_initialize();
			usart_initialize();
			iwdg_initialize();
			timer_initialize();		
			//usart_putstring(SERVER_UART_INDEX,"Hello world1\r\n",14);
			usr_k3_reset();
			//usart_putstring(CARD_UART_INDEX,"Hello world2\r\n",14);
			main_control.main_state = MAIN_READY_STATE;
		break;
		case MAIN_READY_STATE:
			if(usart_get_rx_flag() & SERVER_UART_BITMASK)
			{
					main_control.main_state = MAIN_SERVER_STATE;
			}
			if(usart_get_rx_flag() & CARD_UART_BITMASK)
			{
					main_control.main_state = MAIN_CARD_STATE;
			}
#ifdef	AUTO_READ_CARD_MODE
			if(main_control.read_card_flag)
			{
					main_control.main_state = MAIN_READ_CARD_STATE;
					main_control.read_card_flag = 0;
			}
#endif
			iwdg_reload();
		break;
		case MAIN_SERVER_STATE:
			usart_get_data(SERVER_UART_INDEX,&main_usart);
			if(main_usart.target_type == CARD_TARGET)
			{
					usart_putstring(CARD_UART_INDEX,main_usart.buff,main_usart.size);
			}
			usart_data_free(&main_usart);
			main_control.main_state = MAIN_READY_STATE;
		break;
		case MAIN_CARD_STATE:
			usart_get_data(CARD_UART_INDEX,&main_usart);
			if(main_usart.target_type == SERVER_TARGET)
			{
#ifdef	AUTO_VERIFY_CARD_ID_MODE
					if((card_id_verify() == VALID_CARD_ID_STATUS) || card_id_verify() == OTHER_CMD_STATUS)
#else
					if(1)
#endif
					{
							usart_putstring(SERVER_UART_INDEX,main_usart.buff,main_usart.size);
					}
			}
			usart_data_free(&main_usart);
			main_control.main_state = MAIN_READY_STATE;
		break;
#ifdef	AUTO_READ_CARD_MODE
		case MAIN_READ_CARD_STATE:
			main_usart.buff[0] = CARD_CMD_HEADER;
			main_usart.buff[1] = 0x02;
			main_usart.buff[2] = READ_CARD_ID_CMD;
			main_usart.buff[3] = 0xB9;		//checksum
			main_usart.size = 4;
			usart_putstring(CARD_UART_INDEX,main_usart.buff,main_usart.size);
			usart_data_free(&main_usart);
			main_control.main_state = MAIN_READY_STATE;
		break;
#endif
		default:
			main_control.main_state = MAIN_INIT_STATE;
			return(CMD_FAIL);
		break;
	}
	return(CMD_SUCCESS);
}

static void prohibitive_card_id_initialize(void)
{
	//prohibitive_card_id[0][] = {};
}

static void variable_initialize(void)
{
	main_control.main_state = MAIN_INIT_STATE;
	main_control.led_status = 0;
#ifdef	AUTO_READ_CARD_MODE
	main_control.read_card_flag = 0;
	memset(main_control.card_buff,0x00,CARD_READ_CMD_SIZE);
#endif
}

int main(void)
{	
	variable_initialize();
	
	while(1)
	{
		main_state_machine();
		//usart_putstring(SERVER_UART_INDEX,"Hello world1\r\n",14);
		//usart_putstring(CARD_UART_INDEX,"Hello world2\r\n",14);
		//Delay_ms(1000);
	}
	return(0);
}
