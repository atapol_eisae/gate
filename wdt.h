#ifndef _WDT_H_
#define _WDT_H_

#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
//#include "stm32fxxx_hal.h"
//#include "defines.h"
//#include "tm_stm32_disco.h"
//#include "tm_stm32_delay.h"
//#include "tm_stm32_iwdg.h"
#include "STM32_Init.h" 
#include "gate_template.h"

typedef enum {
	TM_IWDG_Timeout_5ms = 0x00,   /*!< System reset called every 5ms */
	TM_IWDG_Timeout_10ms = 0x01,  /*!< System reset called every 10ms */
	TM_IWDG_Timeout_15ms = 0x02,  /*!< System reset called every 15ms */
	TM_IWDG_Timeout_30ms = 0x03,  /*!< System reset called every 30ms */
	TM_IWDG_Timeout_60ms = 0x04,  /*!< System reset called every 60ms */
	TM_IWDG_Timeout_120ms = 0x05, /*!< System reset called every 120ms */
	TM_IWDG_Timeout_250ms = 0x06, /*!< System reset called every 250ms */
	TM_IWDG_Timeout_500ms = 0x07, /*!< System reset called every 500ms */
	TM_IWDG_Timeout_1s = 0x08,    /*!< System reset called every 1s */
	TM_IWDG_Timeout_2s = 0x09,    /*!< System reset called every 2s */
	TM_IWDG_Timeout_4s = 0x0A,    /*!< System reset called every 4s */
	TM_IWDG_Timeout_8s = 0x0B,    /*!< System reset called every 8s */
	TM_IWDG_Timeout_16s = 0x0C,   /*!< System reset called every 16s */
	TM_IWDG_Timeout_32s = 0x0D    /*!< System reset called every 32s. This is maximum value allowed with IWDG timer */
} TM_IWDG_Timeout_t;



void iwdg_reload(void);
void iwdg_initialize(void);

#endif
