
#include "uart.h"


//static control_var_t control_usart;
static usart_var_t server_usart;
static usart_var_t card_usart;
static control_var_t uart_control;

//static char dbuff[100];

#ifdef GATE_CARD_BEACON
	static usart_var_t beacon_usart;
#endif

void USART1_IRQHandler(void)		//usart for usrk3
{
	char rx_ch;
	/* RXNE handler */
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
			rx_ch = (char)USART_ReceiveData(USART1);
			
			if((rx_ch == (char)0xBA) && (server_usart.idx == 0))
			{
					//USART_SendData(USART1,'1');
					server_usart.target_type = CARD_TARGET;
			}
			else if(server_usart.idx == 0)
			{
					server_usart.idx = 0;
			}
			
			if(server_usart.target_type == CARD_TARGET)
			{
					server_usart.tmp_buff[server_usart.idx] = rx_ch;
				
					if(server_usart.idx == CARD_CMD_SIZE_BYTE)	//Get data size
					{
							server_usart.size = server_usart.tmp_buff[server_usart.idx];
					}
					server_usart.idx++;
					
					if(server_usart.idx >= (server_usart.size + CARD_OFFSET_SIZE))
					{
							server_usart.tmp_buff[server_usart.idx] = '\0';
							memcpy(server_usart.buff,server_usart.tmp_buff,server_usart.idx);
							server_usart.size = server_usart.idx;
							server_usart.rx_flag = 1;
							server_usart.idx = 0;
							memset(server_usart.tmp_buff,(char)0x00,USART_BUFF_SIZE);
							server_usart.target_type = CARD_TARGET;
					}
			}
			/* Wait until Tx data register is empty, not really 
			 * required for this example but put in here anyway.
			 */
			while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
			{
			}
	}
	 
	/* ------------------------------------------------------------ */
	/* Other USART1 interrupts handler can go here ...             */
}  

void USART2_IRQHandler(void)	//usart for card reader
{
		char rx_ch;
    /* RXNE handler */
    if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
    {
				rx_ch = (char)USART_ReceiveData(USART2);
				card_usart.tmp_buff[card_usart.idx] = rx_ch;
				
				
				if(card_usart.idx == CARD_CMD_SIZE_BYTE)	//Get data size
				{
						card_usart.size = card_usart.tmp_buff[card_usart.idx];
						//USART_SendData(USART2,card_usart.size);
				}
				card_usart.idx++;
				
				if(card_usart.idx >= card_usart.size + CARD_OFFSET_SIZE)
				{
						card_usart.tmp_buff[card_usart.idx] = '\0';
						memcpy(card_usart.buff,card_usart.tmp_buff,card_usart.size + CARD_OFFSET_SIZE);
						card_usart.target_type = SERVER_TARGET;
						card_usart.size	= card_usart.idx;
						card_usart.rx_flag = 1;
						card_usart.idx = 0;
						memset(card_usart.tmp_buff,(char)0x00,USART_BUFF_SIZE);
					  
				}
				/* Wait until Tx data register is empty, not really 
				 * required for this example but put in here anyway.
				 */
				
				while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
				{
				}
    }
     
    /* ------------------------------------------------------------ */
    /* Other USART2 interrupts handler can go here ...             */
}

#ifdef GATE_CARD_BEACON
	void USART3_IRQHandler(void)
	{
			char rx_ch;
			/* RXNE handler */
			if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
			{
					USART_SendData(USART1, (char)USART_ReceiveData(USART3));
					/* Wait until Tx data register is empty, not really 
					 * required for this example but put in here anyway.
					 */
					/*
					while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
					{
					}*/
			}
			 
			/* ------------------------------------------------------------ */
			/* Other USART3 interrupts handler can go here ...             */
	}
#endif

void usart_nvic_configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable the USARTx Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_Init(&NVIC_InitStructure);
}

void usart_configuration(void)
{

  USART_InitTypeDef USART_InitStructure;

  USART_InitStructure.USART_BaudRate = 115200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl =     USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;   
  USART_Init(USART1, &USART_InitStructure);
	USART_Init(USART2, &USART_InitStructure);
  
	USART_Cmd(USART1, ENABLE);
	/* Enable USART2 */
  USART_Cmd(USART2, ENABLE);



  usart_nvic_configuration();
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    /* Enable the USART2 Receive interrupt: this interrupt is generated when the USART2 receive data register is not empty */
  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
}

void usart_gpio_config(void)
 {
   GPIO_InitTypeDef GPIO_InitStructure;
	 USART_InitTypeDef uart_init;	 
   // enable uart 1 preph clock mode
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA ,     ENABLE);

    // enable uart 2 preph clock mode
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);

 /* Configure USART1 Tx (PA.09) and UARTT2 Tx (PA.02) as alternate        function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(USART_PORT, &GPIO_InitStructure);

  /* Configure USART1 Rx (PA.10) and USART2 Rx (PA.03) as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_3;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(USART_PORT, &GPIO_InitStructure);
	 
}

void usart_putstring(uint8_t usart_index, char *str, uint16_t str_len)
{
		uint16_t idx;
		USART_TypeDef* USARTx;
		
		switch(usart_index)
		{
			case SERVER_UART_INDEX:
				USARTx = USART1;
			break;
			case CARD_UART_INDEX:
				USARTx = USART2;
			break;
			default:
				
			break;
		}
		for(idx=0;idx<str_len;idx++)
		{
				USART_SendData(USARTx,(char)str[idx]);
				while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET)
				{
				}
		}
}

uint8_t usart_get_rx_flag(void)
{
	uint8_t rx_flag = 0;
	
	rx_flag = (card_usart.rx_flag << CARD_UART_INDEX) | (server_usart.rx_flag << SERVER_UART_INDEX);
	//sprintf(dbuff,"RX Flag = %X\r\n",rx_flag);
	//usart_putstring(SERVER_UART_INDEX,dbuff,strlen(dbuff));
	//sprintf(dbuff,"sever size = %d\r\n",server_usart.size);
	//usart_putstring(SERVER_UART_INDEX,dbuff,strlen(dbuff));
	//usart_putstring(SERVER_UART_INDEX,server_usart.buff,4);
	return(rx_flag);
}

uint8_t usart_get_data(uint8_t usart_index,usart_var_t *data_usart)
{
	switch(usart_index)
	{
		case SERVER_UART_INDEX: 
			if(server_usart.rx_flag)
			{
				//sprintf(dbuff,"RX Flag = %X\r\n",server_usart.rx_flag);
				//usart_putstring(SERVER_UART_INDEX,dbuff,strlen(dbuff));
				//memcpy(str,server_usart.buff,server_usart.size + CARD_OFFSET_SIZE);
				*data_usart = server_usart;
				
				server_usart.rx_flag = 0;
				server_usart.size = 0;
				memset(server_usart.buff,0x00,USART_BUFF_SIZE);
				server_usart.target_type = UNIDENTIFY_TARGET;
			}
			else
			{
				return(CMD_FAIL);
			}
		break;
		case CARD_UART_INDEX:
			if(card_usart.rx_flag)
			{
				//memcpy(str,card_usart.buff,card_usart.size + CARD_OFFSET_SIZE);
				*data_usart = card_usart;
				card_usart.rx_flag = 0;
				memset(card_usart.buff,0x00,USART_BUFF_SIZE);
				server_usart.target_type = UNIDENTIFY_TARGET;
			}
			else
			{
				return(CMD_FAIL);
			}
		break;
		default:
				return(CMD_FAIL);
		break;
	}
	
	return(CMD_SUCCESS);
}

void usart_data_free(usart_var_t *data_usart)
{
	data_usart->idx = 0;
	data_usart->rx_flag = 0;
	data_usart->size = 0;
	memset(data_usart->buff,0x00,USART_BUFF_SIZE);
	memset(data_usart->tmp_buff,0x00,USART_BUFF_SIZE);
}
 
void usart_variable_initialize(void)
{
	usart_var_t template_usart;
	
	template_usart.idx = 0;
	template_usart.rx_flag = 0;
	template_usart.size = 0;
	memset(template_usart.buff,0x00,USART_BUFF_SIZE);
	memset(template_usart.tmp_buff,0x00,USART_BUFF_SIZE);
	
	server_usart = template_usart;
	card_usart = template_usart;
	uart_control.busy_flag = 0;
	
	server_usart.target_type = UNIDENTIFY_TARGET;
}

void usart_initialize(void)
{
		usart_gpio_config();
		usart_configuration();
		usart_variable_initialize();
}