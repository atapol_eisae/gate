#include "wdt.h"

void iwdg_reload(void)
{
		IWDG->KR = 0xAAAA;    //Reload IWDG
}
 
void iwdg_initialize(void)
{
     IWDG->KR = 0x5555;    //Disable write protection of IWDG registers
     IWDG->PR = 0x06;      //Set PR value
     IWDG->RLR = 384;	//0xB4;     //Set RLR value
     IWDG->KR = 0xAAAA;    //Reload IWDG
     IWDG->KR = 0xCCCC;    //Start IWDG
}
