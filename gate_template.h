#ifndef _GATE_TEMPLATE_H
#define _GATE_TEMPLATE_H

#define CMD_FAIL			0
#define CMD_SUCCESS		1

#define AUTO_READ_CARD_MODE					//This mode will read the card ID periodically
#define AUTO_VERIFY_CARD_ID_MODE		//This mode will not send the card id to the server if no card is detected

#define GATE_CARD
//#define BEACON_CARD_GATE 
	#define USART_USR_TX_PIN		GPIO_Pin_9		//Port A
	#define USART_USR_RX_PIN		GPIO_Pin_10		//Port A
	#define USR_RESET_PIN				GPIO_Pin_11		//Port A
	#define USR_RESET_PORT			GPIOA
	#define CARD_TX_PIN					GPIO_Pin_2		//Port A
	#define CARD_RX_PIN					GPIO_Pin_3		//Port A
	#define USART_PORT					GPIOA
	#define LED_STATUS_PIN			GPIO_Pin_13		//Port C
	#define LED_STATUS_PORT			GPIOC

	#ifdef BEACON_CARD_GATE

	#else
	
	#endif

#endif
