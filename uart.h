#ifndef _UART_H_
#define _UART_H_

#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "STM32_Init.h" 
#include "gate_template.h"
#include "mifare_card.h"

#define USART_BUFF_SIZE	256

typedef enum
{
	UNIDENTIFY_TARGET,
	SERVER_TARGET,
	CARD_TARGET,
	BEACON_TARGET
}
target_type_e;

typedef enum
{
	UNIDENTIFY_UART_INDEX,
	SERVER_UART_INDEX,
	CARD_UART_INDEX,
	BEACON_UART_INDEX
}
uart_type_index_e;

typedef enum
{
	UNIDENTIFY_UART_BITMASK = (1 << UNIDENTIFY_UART_INDEX),
	SERVER_UART_BITMASK = (1 << SERVER_UART_INDEX),
	CARD_UART_BITMASK = (1 << CARD_UART_INDEX),
	BEACON_UART_BITMASK = (1 << BEACON_UART_INDEX)
}
uart_type_bitmask_e;

typedef struct
{
	char tmp_buff[USART_BUFF_SIZE];
	char buff[USART_BUFF_SIZE];
	uint8_t rx_flag;
	uint8_t tx_flag;
	uint16_t idx;
	uint16_t size;
	uint8_t target_type;
}
usart_var_t;

typedef struct
{
		uint8_t busy_flag;
}
control_var_t;

void usart_initialize(void);
void usart_putstring(uint8_t usart_index, char *str, uint16_t str_len);
uint8_t usart_get_rx_flag(void);
uint8_t usart_get_data(uint8_t usart_index,usart_var_t *data_usart);
void usart_data_free(usart_var_t *data_usart);

#endif
