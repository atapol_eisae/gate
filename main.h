
#ifndef _MAIN_H_
#define _MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "STM32_Init.h" 
#include "string.h"
#include "uart.h"
#include "mifare_card.h"
#include "wdt.h"

typedef enum
{
	MAIN_INIT_STATE,
	MAIN_READY_STATE,
	MAIN_SERVER_STATE,
	MAIN_CARD_STATE,
	MAIN_READ_CARD_STATE
}
main_state_e;

typedef struct
{
	uint8_t main_state;
	uint8_t led_status;
#ifdef	AUTO_READ_CARD_MODE
	uint8_t read_card_flag;
	char card_buff[CARD_READ_CMD_SIZE];
#endif
}
main_control_var_t;

#endif
